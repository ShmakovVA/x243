import pytest


@pytest.fixture
def client():
    from services.web.project import app
    app.config['TESTING'] = True
    return app.test_client()
