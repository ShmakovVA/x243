from flask import request, Flask, jsonify
import json
import os
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object("project.config.Config")
db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)

    def __init__(self, email):
        self.email = email


@app.route("/")
def hello_world():
    return jsonify(hello="world 4")


@app.route('/plus_one')
def plus_one():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 1})


@app.route('/plus_two')
def plus_two():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 2})


@app.route('/plus_10')
def plus_10():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 10})


@app.route('/square')
def square():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x * x})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
